<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\CommentType;

class DefaultController extends Controller
{
	
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
    	
       $qb = $this->getDoctrine()
       			->getManager()
       			->createQuerybuilder()
       			->from('AppBundle:Post', 'p')
       			->select('p');
       			
       $paginator = $this->get('knp_paginator');
       $pagination = $paginator->paginate(
       			$qb,
       			$request->query->get('page', 1),
       			20
       		);
    	
        return $this->render('default/index.html.twig', array(
        			'posts' => $pagination
        			));
          
    }
    	
    
    
    

    /**
     * @Route("/artykul/{id}", name="post_show")
     */
    public function showFormAction(Post $post)
    {
    	$form = $this->createForm(CommentType::class);
    	
    	 return $this->render('default/show.html.twig', [
    			'post' => $post,
    	 		'form' => $form->createView()
        ]);
    }
    
    
    
 }
